-- 1
SELECT `customerName` FROM `customers` WHERE `country` LIKE 'Philippines';

-- 2
SELECT `contactLastName`, `contactFirstName` FROM `customers` WHERE `customerName` = 'La Rochelle Gifts';

-- 3
SELECT `productName`, `MSRP` FROM `products` WHERE `productName` = 'The Titanic';

-- 4
SELECT `firstName`, `lastName` FROM `employees` WHERE `email` = 'jfirrelli@classicmodelcars.com';

-- 5
SELECT `customerName` FROM `customers` WHERE `state` IS NULL;

-- 6
SELECT `firstName`, `lastName` FROM `employees` WHERE `lastName` = 'Patterson' AND `firstName` = 'Steve';

-- 7
SELECT `customerName`, `country`, `creditLimit` FROM `customers` WHERE `country` != 'USA' AND `creditLimit` > 3000;

-- 8
SELECT `customerNumber` FROM `orders` WHERE `comments` LIKE '%DHL%';

-- 9
SELECT `productLine` FROM `productlines` WHERE `textDescription` LIKE '%state of the art%';

-- 10
SELECT `country` FROM `customers` GROUP BY `country`;

-- 11
SELECT `status` FROM `orders` GROUP BY `status`;

-- 12
SELECT `customerName`, `country` FROM `customers` WHERE `country` IN ('USA', 'France', 'Canada');

-- 13
SELECT emp.`firstName`, emp.`lastName` FROM `employees` emp JOIN `offices` o ON emp.`officeCode`= o.`officeCode` WHERE o.`city`='Tokyo';

-- 14
SELECT cust.`customerName` FROM `customers` cust JOIN `employees` emp ON cust.`salesRepEmployeeNumber`= emp.`employeeNumber` WHERE emp.`firstName` = 'Leslie' AND `lastName` = 'Thompson';

-- 15
SELECT `productName`, `customerName` FROM `orderdetails`  
JOIN `orders` ON orderdetails.`orderNumber`= orders.`orderNumber` 
JOIN `products` ON orderdetails.`productCode`= products.`productCode`
JOIN `customers` ON orders.`customerNumber`= customers.`customerNumber` 
WHERE `customerName` = 'Baane Mini Imports';

-- 16
SELECT emp.`firstName`, emp.`lastName`, cust.`customerName`, o.`country` FROM `customers` cust 
JOIN `employees` emp ON cust.`salesRepEmployeeNumber`= emp.`employeeNumber` 
JOIN `offices` o ON cust.`country`= o.`country`;

-- 17
SELECT products.`productName`, products.`quantityInStock` FROM `products`
JOIN `productLines` ON products.`productLine`= productLines.`productLine`
WHERE  productLines.`productLine` = 'planes' AND `quantityInStock`  < 1000;
-- or
SELECT `productName`, `quantityInStock` FROM `products` WHERE `productLine` = 'planes' AND `quantityInStock` < 1000

-- 18
SELECT `customerName` FROM `customers` WHERE `phone` LIKE '%+81%';